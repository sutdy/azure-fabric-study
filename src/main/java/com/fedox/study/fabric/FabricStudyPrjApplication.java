package com.fedox.study.fabric;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FabricStudyPrjApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabricStudyPrjApplication.class, args);
	}

}
